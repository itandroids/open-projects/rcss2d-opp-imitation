BEGIN;

SET SCHEMA ; -- PUT YOUR SCHEMA NAME HERE!

CREATE OR REPLACE FUNCTION get_matchstate_playerstates(matchstate_id int) 
RETURNS TABLE (
	matchstate_id_fk int,
	unum int,
	teamname varchar(32),
 	x numeric,
 	y numeric,
 	body numeric,
 	vx numeric,
 	vy numeric,
 	dash_power_rate numeric,
	effort_min numeric,
 	effort_max numeric,
 	extra_stamina numeric,
 	inertia_moment numeric,
 	kick_rand numeric,
 	kickable_margin numeric,
 	player_decay numeric
) 
LANGUAGE SQL
BEGIN ATOMIC
	SELECT
		matchstate_id_fk,
		unum,
		teamname,
		x,
		y,
		body,
		vx,
		vy,
		pt.dash_power_rate as dash_power_rate,
		pt.effort_min as effort_min,
		pt.effort_max as effort_max,
		pt.extra_stamina as extra_stamina,
		pt.inertia_moment as inertia_moment,
		pt.kick_rand as kick_rand,
		pt.kickable_margin as kickable_margin,
		pt.player_decay as player_decay
	FROM playerstates as ps
		JOIN playertypes as pt
			ON ps.playertype_id_fk = pt.playertype_id
	WHERE matchstate_id_fk = matchstate_id
	LIMIT 22;
END;

CREATE OR REPLACE FUNCTION get_batchof_condensed_matchstates(batch_size int) 
RETURNS TABLE (
	matchstate_id int,
	left_teamname varchar(32),
    right_teamname varchar(32),
    cycle int,
    ball_x numeric,
    ball_y numeric,
    ball_vx numeric,
    ball_vy numeric,
 	l1_x numeric,
 	l1_y numeric,
 	l1_body numeric,
 	l1_vx numeric,
 	l1_vy numeric,
 	l1_dash_power_rate numeric,
	l1_effort_min numeric,
 	l1_effort_max numeric,
 	l1_extra_stamina numeric,
 	l1_inertia_moment numeric,
 	l1_kick_rand numeric,
 	l1_kickable_margin numeric,
 	l1_player_decay numeric,
 	l2_x numeric,
 	l2_y numeric,
 	l2_body numeric,
 	l2_vx numeric,
 	l2_vy numeric,
 	l2_dash_power_rate numeric,
	l2_effort_min numeric,
 	l2_effort_max numeric,
 	l2_extra_stamina numeric,
 	l2_inertia_moment numeric,
 	l2_kick_rand numeric,
 	l2_kickable_margin numeric,
 	l2_player_decay numeric,
 	l3_x numeric,
 	l3_y numeric,
 	l3_body numeric,
 	l3_vx numeric,
 	l3_vy numeric,
 	l3_dash_power_rate numeric,
	l3_effort_min numeric,
 	l3_effort_max numeric,
 	l3_extra_stamina numeric,
 	l3_inertia_moment numeric,
 	l3_kick_rand numeric,
 	l3_kickable_margin numeric,
 	l3_player_decay numeric,
 	l4_x numeric,
 	l4_y numeric,
 	l4_body numeric,
 	l4_vx numeric,
 	l4_vy numeric,
 	l4_dash_power_rate numeric,
	l4_effort_min numeric,
 	l4_effort_max numeric,
 	l4_extra_stamina numeric,
 	l4_inertia_moment numeric,
 	l4_kick_rand numeric,
 	l4_kickable_margin numeric,
 	l4_player_decay numeric,
 	l5_x numeric,
 	l5_y numeric,
 	l5_body numeric,
 	l5_vx numeric,
 	l5_vy numeric,
 	l5_dash_power_rate numeric,
	l5_effort_min numeric,
 	l5_effort_max numeric,
 	l5_extra_stamina numeric,
 	l5_inertia_moment numeric,
 	l5_kick_rand numeric,
 	l5_kickable_margin numeric,
 	l5_player_decay numeric,
 	l6_x numeric,
 	l6_y numeric,
 	l6_body numeric,
 	l6_vx numeric,
 	l6_vy numeric,
 	l6_dash_power_rate numeric,
	l6_effort_min numeric,
 	l6_effort_max numeric,
 	l6_extra_stamina numeric,
 	l6_inertia_moment numeric,
 	l6_kick_rand numeric,
 	l6_kickable_margin numeric,
 	l6_player_decay numeric,
 	l7_x numeric,
 	l7_y numeric,
 	l7_body numeric,
 	l7_vx numeric,
 	l7_vy numeric,
 	l7_dash_power_rate numeric,
	l7_effort_min numeric,
 	l7_effort_max numeric,
 	l7_extra_stamina numeric,
 	l7_inertia_moment numeric,
 	l7_kick_rand numeric,
 	l7_kickable_margin numeric,
 	l7_player_decay numeric,
 	l8_x numeric,
 	l8_y numeric,
 	l8_body numeric,
 	l8_vx numeric,
 	l8_vy numeric,
 	l8_dash_power_rate numeric,
	l8_effort_min numeric,
 	l8_effort_max numeric,
 	l8_extra_stamina numeric,
 	l8_inertia_moment numeric,
 	l8_kick_rand numeric,
 	l8_kickable_margin numeric,
 	l8_player_decay numeric,
 	l9_x numeric,
 	l9_y numeric,
 	l9_body numeric,
 	l9_vx numeric,
 	l9_vy numeric,
 	l9_dash_power_rate numeric,
	l9_effort_min numeric,
 	l9_effort_max numeric,
 	l9_extra_stamina numeric,
 	l9_inertia_moment numeric,
 	l9_kick_rand numeric,
 	l9_kickable_margin numeric,
 	l9_player_decay numeric,
 	l10_x numeric,
 	l10_y numeric,
 	l10_body numeric,
 	l10_vx numeric,
 	l10_vy numeric,
 	l10_dash_power_rate numeric,
	l10_effort_min numeric,
 	l10_effort_max numeric,
 	l10_extra_stamina numeric,
 	l10_inertia_moment numeric,
 	l10_kick_rand numeric,
 	l10_kickable_margin numeric,
 	l10_player_decay numeric,
 	l11_x numeric,
 	l11_y numeric,
 	l11_body numeric,
 	l11_vx numeric,
 	l11_vy numeric,
 	l11_dash_power_rate numeric,
	l11_effort_min numeric,
 	l11_effort_max numeric,
 	l11_extra_stamina numeric,
 	l11_inertia_moment numeric,
 	l11_kick_rand numeric,
 	l11_kickable_margin numeric,
 	l11_player_decay numeric,
 	r1_x numeric,
 	r1_y numeric,
 	r1_body numeric,
 	r1_vx numeric,
 	r1_vy numeric,
 	r1_dash_power_rate numeric,
	r1_effort_min numeric,
 	r1_effort_max numeric,
 	r1_extra_stamina numeric,
 	r1_inertia_moment numeric,
 	r1_kick_rand numeric,
 	r1_kickable_margin numeric,
 	r1_player_decay numeric,
 	r2_x numeric,
 	r2_y numeric,
 	r2_body numeric,
 	r2_vx numeric,
 	r2_vy numeric,
 	r2_dash_power_rate numeric,
	r2_effort_min numeric,
 	r2_effort_max numeric,
 	r2_extra_stamina numeric,
 	r2_inertia_moment numeric,
 	r2_kick_rand numeric,
 	r2_kickable_margin numeric,
 	r2_player_decay numeric,
 	r3_x numeric,
 	r3_y numeric,
 	r3_body numeric,
 	r3_vx numeric,
 	r3_vy numeric,
 	r3_dash_power_rate numeric,
	r3_effort_min numeric,
 	r3_effort_max numeric,
 	r3_extra_stamina numeric,
 	r3_inertia_moment numeric,
 	r3_kick_rand numeric,
 	r3_kickable_margin numeric,
 	r3_player_decay numeric,
 	r4_x numeric,
 	r4_y numeric,
 	r4_body numeric,
 	r4_vx numeric,
 	r4_vy numeric,
 	r4_dash_power_rate numeric,
	r4_effort_min numeric,
 	r4_effort_max numeric,
 	r4_extra_stamina numeric,
 	r4_inertia_moment numeric,
 	r4_kick_rand numeric,
 	r4_kickable_margin numeric,
 	r4_player_decay numeric,
 	r5_x numeric,
 	r5_y numeric,
 	r5_body numeric,
 	r5_vx numeric,
 	r5_vy numeric,
 	r5_dash_power_rate numeric,
	r5_effort_min numeric,
 	r5_effort_max numeric,
 	r5_extra_stamina numeric,
 	r5_inertia_moment numeric,
 	r5_kick_rand numeric,
 	r5_kickable_margin numeric,
 	r5_player_decay numeric,
 	r6_x numeric,
 	r6_y numeric,
 	r6_body numeric,
 	r6_vx numeric,
 	r6_vy numeric,
 	r6_dash_power_rate numeric,
	r6_effort_min numeric,
 	r6_effort_max numeric,
 	r6_extra_stamina numeric,
 	r6_inertia_moment numeric,
 	r6_kick_rand numeric,
 	r6_kickable_margin numeric,
 	r6_player_decay numeric,
 	r7_x numeric,
 	r7_y numeric,
 	r7_body numeric,
 	r7_vx numeric,
 	r7_vy numeric,
 	r7_dash_power_rate numeric,
	r7_effort_min numeric,
 	r7_effort_max numeric,
 	r7_extra_stamina numeric,
 	r7_inertia_moment numeric,
 	r7_kick_rand numeric,
 	r7_kickable_margin numeric,
 	r7_player_decay numeric,
 	r8_x numeric,
 	r8_y numeric,
 	r8_body numeric,
 	r8_vx numeric,
 	r8_vy numeric,
 	r8_dash_power_rate numeric,
	r8_effort_min numeric,
 	r8_effort_max numeric,
 	r8_extra_stamina numeric,
 	r8_inertia_moment numeric,
 	r8_kick_rand numeric,
 	r8_kickable_margin numeric,
 	r8_player_decay numeric,
 	r9_x numeric,
 	r9_y numeric,
 	r9_body numeric,
 	r9_vx numeric,
 	r9_vy numeric,
 	r9_dash_power_rate numeric,
	r9_effort_min numeric,
 	r9_effort_max numeric,
 	r9_extra_stamina numeric,
 	r9_inertia_moment numeric,
 	r9_kick_rand numeric,
 	r9_kickable_margin numeric,
 	r9_player_decay numeric,
 	r10_x numeric,
 	r10_y numeric,
 	r10_body numeric,
 	r10_vx numeric,
 	r10_vy numeric,
 	r10_dash_power_rate numeric,
	r10_effort_min numeric,
 	r10_effort_max numeric,
 	r10_extra_stamina numeric,
 	r10_inertia_moment numeric,
 	r10_kick_rand numeric,
 	r10_kickable_margin numeric,
 	r10_player_decay numeric,
 	r11_x numeric,
 	r11_y numeric,
 	r11_body numeric,
 	r11_vx numeric,
 	r11_vy numeric,
 	r11_dash_power_rate numeric,
	r11_effort_min numeric,
 	r11_effort_max numeric,
 	r11_extra_stamina numeric,
 	r11_inertia_moment numeric,
 	r11_kick_rand numeric,
 	r11_kickable_margin numeric,
 	r11_player_decay numeric
) 
LANGUAGE SQL
BEGIN ATOMIC
SELECT
	ms.matchstate_id,
	ms.left_teamname,
	ms.right_teamname,
	ms.cycle,
 	ms.ball_x,
 	ms.ball_y,
 	ms.ball_vx,
 	ms.ball_vy,
	min(x) FILTER (WHERE players.teamname = left_teamname AND players.unum = 1 ) as l1_x,
    min(y) FILTER (WHERE players.teamname = left_teamname AND players.unum = 1 ) as l1_y,
 	min(body) FILTER (WHERE players.teamname = left_teamname AND players.unum = 1 ) as l1_body,
	min(vx) FILTER (WHERE players.teamname = left_teamname AND players.unum = 1 ) as l1_vx,
 	min(vy) FILTER (WHERE players.teamname = left_teamname AND players.unum = 1 ) as l1_vy,
	min(dash_power_rate) FILTER (WHERE players.teamname = left_teamname AND players.unum = 1 ) as l1_dash_power_rate,
	min(effort_min) FILTER (WHERE players.teamname = left_teamname AND players.unum = 1 ) as l1_effort_min,
 	min(effort_max) FILTER (WHERE players.teamname = left_teamname AND players.unum = 1 ) as l1_effort_max,
 	min(extra_stamina) FILTER (WHERE players.teamname = left_teamname AND players.unum = 1 ) as l1_extra_stamina,
 	min(inertia_moment) FILTER (WHERE players.teamname = left_teamname AND players.unum = 1 ) as l1_inertia_moment,
 	min(kick_rand) FILTER (WHERE players.teamname = left_teamname AND players.unum = 1 ) as l1_kick_rand,
 	min(kickable_margin) FILTER (WHERE players.teamname = left_teamname AND players.unum = 1 ) as l1_kickable_margin,
 	min(player_decay) FILTER (WHERE players.teamname = left_teamname AND players.unum = 1 ) as l1_player_decay,
    min(x) FILTER (WHERE players.teamname = left_teamname AND players.unum = 2 ) as l2_x,
	min(y) FILTER (WHERE players.teamname = left_teamname AND players.unum = 2 ) as l2_y,
 	min(body) FILTER (WHERE players.teamname = left_teamname AND players.unum = 2 ) as l2_body,
	min(vx) FILTER (WHERE players.teamname = left_teamname AND players.unum = 2 ) as l2_vx,
 	min(vy) FILTER (WHERE players.teamname = left_teamname AND players.unum = 2 ) as l2_vy,
	min(dash_power_rate) FILTER (WHERE players.teamname = left_teamname AND players.unum = 2 ) as l2_dash_power_rate,
	min(effort_min) FILTER (WHERE players.teamname = left_teamname AND players.unum = 2 ) as l2_effort_min,
 	min(effort_max) FILTER (WHERE players.teamname = left_teamname AND players.unum = 2 ) as l2_effort_max,
 	min(extra_stamina) FILTER (WHERE players.teamname = left_teamname AND players.unum = 2 ) as l2_extra_stamina,
 	min(inertia_moment) FILTER (WHERE players.teamname = left_teamname AND players.unum = 2 ) as l2_inertia_moment,
 	min(kick_rand) FILTER (WHERE players.teamname = left_teamname AND players.unum = 2 ) as l2_kick_rand,
 	min(kickable_margin) FILTER (WHERE players.teamname = left_teamname AND players.unum = 2 ) as l2_kickable_margin,
 	min(player_decay) FILTER (WHERE players.teamname = left_teamname AND players.unum = 2 ) as l2_player_decay,
	min(x) FILTER (WHERE players.teamname = left_teamname AND players.unum = 3 ) as l3_x,
	min(y) FILTER (WHERE players.teamname = left_teamname AND players.unum = 3 ) as l3_y,
 	min(body) FILTER (WHERE players.teamname = left_teamname AND players.unum = 3 ) as l3_body,
	min(vx) FILTER (WHERE players.teamname = left_teamname AND players.unum = 3 ) as l3_vx,
 	min(vy) FILTER (WHERE players.teamname = left_teamname AND players.unum = 3 ) as l3_vy,
	min(dash_power_rate) FILTER (WHERE players.teamname = left_teamname AND players.unum = 3 ) as l3_dash_power_rate,
	min(effort_min) FILTER (WHERE players.teamname = left_teamname AND players.unum = 3 ) as l3_effort_min,
 	min(effort_max) FILTER (WHERE players.teamname = left_teamname AND players.unum = 3 ) as l3_effort_max,
 	min(extra_stamina) FILTER (WHERE players.teamname = left_teamname AND players.unum = 3 ) as l3_extra_stamina,
 	min(inertia_moment) FILTER (WHERE players.teamname = left_teamname AND players.unum = 3 ) as l3_inertia_moment,
 	min(kick_rand) FILTER (WHERE players.teamname = left_teamname AND players.unum = 3 ) as l3_kick_rand,
 	min(kickable_margin) FILTER (WHERE players.teamname = left_teamname AND players.unum = 3 ) as l3_kickable_margin,
 	min(player_decay) FILTER (WHERE players.teamname = left_teamname AND players.unum = 3 ) as l3_player_decay,
	min(x) FILTER (WHERE players.teamname = left_teamname AND players.unum = 4 ) as l4_x,
	min(y) FILTER (WHERE players.teamname = left_teamname AND players.unum = 4 ) as l4_y,
 	min(body) FILTER (WHERE players.teamname = left_teamname AND players.unum = 4 ) as l4_body,
	min(vx) FILTER (WHERE players.teamname = left_teamname AND players.unum = 4 ) as l4_vx,
 	min(vy) FILTER (WHERE players.teamname = left_teamname AND players.unum = 4 ) as l4_vy,
	min(dash_power_rate) FILTER (WHERE players.teamname = left_teamname AND players.unum = 4 ) as l4_dash_power_rate,
	min(effort_min) FILTER (WHERE players.teamname = left_teamname AND players.unum = 4 ) as l4_effort_min,
 	min(effort_max) FILTER (WHERE players.teamname = left_teamname AND players.unum = 4 ) as l4_effort_max,
 	min(extra_stamina) FILTER (WHERE players.teamname = left_teamname AND players.unum = 4 ) as l4_extra_stamina,
 	min(inertia_moment) FILTER (WHERE players.teamname = left_teamname AND players.unum = 4 ) as l4_inertia_moment,
 	min(kick_rand) FILTER (WHERE players.teamname = left_teamname AND players.unum = 4 ) as l4_kick_rand,
 	min(kickable_margin) FILTER (WHERE players.teamname = left_teamname AND players.unum = 4 ) as l4_kickable_margin,
 	min(player_decay) FILTER (WHERE players.teamname = left_teamname AND players.unum = 4 ) as l4_player_decay,
	min(x) FILTER (WHERE players.teamname = left_teamname AND players.unum = 5 ) as l5_x,
	min(y) FILTER (WHERE players.teamname = left_teamname AND players.unum = 5 ) as l5_y,
 	min(body) FILTER (WHERE players.teamname = left_teamname AND players.unum = 5 ) as l5_body,
	min(vx) FILTER (WHERE players.teamname = left_teamname AND players.unum = 5 ) as l5_vx,
 	min(vy) FILTER (WHERE players.teamname = left_teamname AND players.unum = 5 ) as l5_vy,
	min(dash_power_rate) FILTER (WHERE players.teamname = left_teamname AND players.unum = 5 ) as l5_dash_power_rate,
	min(effort_min) FILTER (WHERE players.teamname = left_teamname AND players.unum = 5 ) as l5_effort_min,
 	min(effort_max) FILTER (WHERE players.teamname = left_teamname AND players.unum = 5 ) as l5_effort_max,
 	min(extra_stamina) FILTER (WHERE players.teamname = left_teamname AND players.unum = 5 ) as l5_extra_stamina,
 	min(inertia_moment) FILTER (WHERE players.teamname = left_teamname AND players.unum = 5 ) as l5_inertia_moment,
 	min(kick_rand) FILTER (WHERE players.teamname = left_teamname AND players.unum = 5 ) as l5_kick_rand,
 	min(kickable_margin) FILTER (WHERE players.teamname = left_teamname AND players.unum = 5 ) as l5_kickable_margin,
 	min(player_decay) FILTER (WHERE players.teamname = left_teamname AND players.unum = 5 ) as l5_player_decay,
	min(x) FILTER (WHERE players.teamname = left_teamname AND players.unum = 6 ) as l6_x,
	min(y) FILTER (WHERE players.teamname = left_teamname AND players.unum = 6 ) as l6_y,
 	min(body) FILTER (WHERE players.teamname = left_teamname AND players.unum = 6 ) as l6_body,
	min(vx) FILTER (WHERE players.teamname = left_teamname AND players.unum = 6 ) as l6_vx,
 	min(vy) FILTER (WHERE players.teamname = left_teamname AND players.unum = 6 ) as l6_vy,
	min(dash_power_rate) FILTER (WHERE players.teamname = left_teamname AND players.unum = 6 ) as l6_dash_power_rate,
	min(effort_min) FILTER (WHERE players.teamname = left_teamname AND players.unum = 6 ) as l6_effort_min,
 	min(effort_max) FILTER (WHERE players.teamname = left_teamname AND players.unum = 6 ) as l6_effort_max,
 	min(extra_stamina) FILTER (WHERE players.teamname = left_teamname AND players.unum = 6 ) as l6_extra_stamina,
 	min(inertia_moment) FILTER (WHERE players.teamname = left_teamname AND players.unum = 6 ) as l6_inertia_moment,
 	min(kick_rand) FILTER (WHERE players.teamname = left_teamname AND players.unum = 6 ) as l6_kick_rand,
 	min(kickable_margin) FILTER (WHERE players.teamname = left_teamname AND players.unum = 6 ) as l6_kickable_margin,
 	min(player_decay) FILTER (WHERE players.teamname = left_teamname AND players.unum = 6 ) as l6_player_decay,
	min(x) FILTER (WHERE players.teamname = left_teamname AND players.unum = 7 ) as l7_x,
	min(y) FILTER (WHERE players.teamname = left_teamname AND players.unum = 7 ) as l7_y,
 	min(body) FILTER (WHERE players.teamname = left_teamname AND players.unum = 7 ) as l7_body,
	min(vx) FILTER (WHERE players.teamname = left_teamname AND players.unum = 7 ) as l7_vx,
 	min(vy) FILTER (WHERE players.teamname = left_teamname AND players.unum = 7 ) as l7_vy,
	min(dash_power_rate) FILTER (WHERE players.teamname = left_teamname AND players.unum = 7 ) as l7_dash_power_rate,
	min(effort_min) FILTER (WHERE players.teamname = left_teamname AND players.unum = 7 ) as l7_effort_min,
 	min(effort_max) FILTER (WHERE players.teamname = left_teamname AND players.unum = 7 ) as l7_effort_max,
 	min(extra_stamina) FILTER (WHERE players.teamname = left_teamname AND players.unum = 7 ) as l7_extra_stamina,
 	min(inertia_moment) FILTER (WHERE players.teamname = left_teamname AND players.unum = 7 ) as l7_inertia_moment,
 	min(kick_rand) FILTER (WHERE players.teamname = left_teamname AND players.unum = 7 ) as l7_kick_rand,
 	min(kickable_margin) FILTER (WHERE players.teamname = left_teamname AND players.unum = 7 ) as l7_kickable_margin,
 	min(player_decay) FILTER (WHERE players.teamname = left_teamname AND players.unum = 7 ) as l7_player_decay,
	min(x) FILTER (WHERE players.teamname = left_teamname AND players.unum = 8 ) as l8_x,
	min(y) FILTER (WHERE players.teamname = left_teamname AND players.unum = 8 ) as l8_y,
 	min(body) FILTER (WHERE players.teamname = left_teamname AND players.unum = 8 ) as l8_body,
	min(vx) FILTER (WHERE players.teamname = left_teamname AND players.unum = 8 ) as l8_vx,
 	min(vy) FILTER (WHERE players.teamname = left_teamname AND players.unum = 8 ) as l8_vy,
	min(dash_power_rate) FILTER (WHERE players.teamname = left_teamname AND players.unum = 8 ) as l8_dash_power_rate,
	min(effort_min) FILTER (WHERE players.teamname = left_teamname AND players.unum = 8 ) as l8_effort_min,
 	min(effort_max) FILTER (WHERE players.teamname = left_teamname AND players.unum = 8 ) as l8_effort_max,
 	min(extra_stamina) FILTER (WHERE players.teamname = left_teamname AND players.unum = 8 ) as l8_extra_stamina,
 	min(inertia_moment) FILTER (WHERE players.teamname = left_teamname AND players.unum = 8 ) as l8_inertia_moment,
 	min(kick_rand) FILTER (WHERE players.teamname = left_teamname AND players.unum = 8 ) as l8_kick_rand,
 	min(kickable_margin) FILTER (WHERE players.teamname = left_teamname AND players.unum = 8 ) as l8_kickable_margin,
 	min(player_decay) FILTER (WHERE players.teamname = left_teamname AND players.unum = 8 ) as l8_player_decay,
	min(x) FILTER (WHERE players.teamname = left_teamname AND players.unum = 9 ) as l9_x,
	min(y) FILTER (WHERE players.teamname = left_teamname AND players.unum = 9 ) as l9_y,
 	min(body) FILTER (WHERE players.teamname = left_teamname AND players.unum = 9 ) as l9_body,
	min(vx) FILTER (WHERE players.teamname = left_teamname AND players.unum = 9 ) as l9_vx,
 	min(vy) FILTER (WHERE players.teamname = left_teamname AND players.unum = 9 ) as l9_vy,
	min(dash_power_rate) FILTER (WHERE players.teamname = left_teamname AND players.unum = 9 ) as l9_dash_power_rate,
	min(effort_min) FILTER (WHERE players.teamname = left_teamname AND players.unum = 9 ) as l9_effort_min,
 	min(effort_max) FILTER (WHERE players.teamname = left_teamname AND players.unum = 9 ) as l9_effort_max,
 	min(extra_stamina) FILTER (WHERE players.teamname = left_teamname AND players.unum = 9 ) as l9_extra_stamina,
 	min(inertia_moment) FILTER (WHERE players.teamname = left_teamname AND players.unum = 9 ) as l9_inertia_moment,
 	min(kick_rand) FILTER (WHERE players.teamname = left_teamname AND players.unum = 9 ) as l9_kick_rand,
 	min(kickable_margin) FILTER (WHERE players.teamname = left_teamname AND players.unum = 9 ) as l9_kickable_margin,
 	min(player_decay) FILTER (WHERE players.teamname = left_teamname AND players.unum = 9 ) as l9_player_decay,
	min(x) FILTER (WHERE players.teamname = left_teamname AND players.unum = 10 ) as l10_x,
	min(y) FILTER (WHERE players.teamname = left_teamname AND players.unum = 10 ) as l10_y,
 	min(body) FILTER (WHERE players.teamname = left_teamname AND players.unum = 10 ) as l10_body,
	min(vx) FILTER (WHERE players.teamname = left_teamname AND players.unum = 10 ) as l10_vx,
 	min(vy) FILTER (WHERE players.teamname = left_teamname AND players.unum = 10 ) as l10_vy,
	min(dash_power_rate) FILTER (WHERE players.teamname = left_teamname AND players.unum = 10 ) as l10_dash_power_rate,
	min(effort_min) FILTER (WHERE players.teamname = left_teamname AND players.unum = 10 ) as l10_effort_min,
 	min(effort_max) FILTER (WHERE players.teamname = left_teamname AND players.unum = 10 ) as l10_effort_max,
 	min(extra_stamina) FILTER (WHERE players.teamname = left_teamname AND players.unum = 10 ) as l10_extra_stamina,
 	min(inertia_moment) FILTER (WHERE players.teamname = left_teamname AND players.unum = 10 ) as l10_inertia_moment,
 	min(kick_rand) FILTER (WHERE players.teamname = left_teamname AND players.unum = 10 ) as l10_kick_rand,
 	min(kickable_margin) FILTER (WHERE players.teamname = left_teamname AND players.unum = 10 ) as l10_kickable_margin,
 	min(player_decay) FILTER (WHERE players.teamname = left_teamname AND players.unum = 10 ) as l10_player_decay,
	min(x) FILTER (WHERE players.teamname = left_teamname AND players.unum = 11 ) as l11_x,
	min(y) FILTER (WHERE players.teamname = left_teamname AND players.unum = 11 ) as l11_y,
 	min(body) FILTER (WHERE players.teamname = left_teamname AND players.unum = 11 ) as l11_body,
	min(vx) FILTER (WHERE players.teamname = left_teamname AND players.unum = 11 ) as l11_vx,
 	min(vy) FILTER (WHERE players.teamname = left_teamname AND players.unum = 11 ) as l11_vy,
	min(dash_power_rate) FILTER (WHERE players.teamname = left_teamname AND players.unum = 11 ) as l11_dash_power_rate,
	min(effort_min) FILTER (WHERE players.teamname = left_teamname AND players.unum = 11 ) as l11_effort_min,
 	min(effort_max) FILTER (WHERE players.teamname = left_teamname AND players.unum = 11 ) as l11_effort_max,
 	min(extra_stamina) FILTER (WHERE players.teamname = left_teamname AND players.unum = 11 ) as l11_extra_stamina,
 	min(inertia_moment) FILTER (WHERE players.teamname = left_teamname AND players.unum = 11 ) as l11_inertia_moment,
 	min(kick_rand) FILTER (WHERE players.teamname = left_teamname AND players.unum = 11 ) as l11_kick_rand,
 	min(kickable_margin) FILTER (WHERE players.teamname = left_teamname AND players.unum = 11 ) as l11_kickable_margin,
 	min(player_decay) FILTER (WHERE players.teamname = left_teamname AND players.unum = 11 ) as l11_player_decay,
	min(x) FILTER (WHERE players.teamname = right_teamname AND players.unum = 1 ) as r1_x,
	min(y) FILTER (WHERE players.teamname = right_teamname AND players.unum = 1 ) as r1_y,
 	min(body) FILTER (WHERE players.teamname = right_teamname AND players.unum = 1 ) as r1_body,
	min(vx) FILTER (WHERE players.teamname = right_teamname AND players.unum = 1 ) as r1_vx,
 	min(vy) FILTER (WHERE players.teamname = right_teamname AND players.unum = 1 ) as r1_vy,
	min(dash_power_rate) FILTER (WHERE players.teamname = right_teamname AND players.unum = 1 ) as r1_dash_power_rate,
	min(effort_min) FILTER (WHERE players.teamname = right_teamname AND players.unum = 1 ) as r1_effort_min,
 	min(effort_max) FILTER (WHERE players.teamname = right_teamname AND players.unum = 1 ) as r1_effort_max,
 	min(extra_stamina) FILTER (WHERE players.teamname = right_teamname AND players.unum = 1 ) as r1_extra_stamina,
 	min(inertia_moment) FILTER (WHERE players.teamname = right_teamname AND players.unum = 1 ) as r1_inertia_moment,
 	min(kick_rand) FILTER (WHERE players.teamname = right_teamname AND players.unum = 1 ) as r1_kick_rand,
 	min(kickable_margin) FILTER (WHERE players.teamname = right_teamname AND players.unum = 1 ) as r1_kickable_margin,
 	min(player_decay) FILTER (WHERE players.teamname = right_teamname AND players.unum = 1 ) as r1_player_decay,
    min(x) FILTER (WHERE players.teamname = right_teamname AND players.unum = 2 ) as r2_x,
	min(y) FILTER (WHERE players.teamname = right_teamname AND players.unum = 2 ) as r2_y,
 	min(body) FILTER (WHERE players.teamname = right_teamname AND players.unum = 2 ) as r2_body,
	min(vx) FILTER (WHERE players.teamname = right_teamname AND players.unum = 2 ) as r2_vx,
 	min(vy) FILTER (WHERE players.teamname = right_teamname AND players.unum = 2 ) as r2_vy,
	min(dash_power_rate) FILTER (WHERE players.teamname = right_teamname AND players.unum = 2 ) as r2_dash_power_rate,
	min(effort_min) FILTER (WHERE players.teamname = right_teamname AND players.unum = 2 ) as r2_effort_min,
 	min(effort_max) FILTER (WHERE players.teamname = right_teamname AND players.unum = 2 ) as r2_effort_max,
 	min(extra_stamina) FILTER (WHERE players.teamname = right_teamname AND players.unum = 2 ) as r2_extra_stamina,
 	min(inertia_moment) FILTER (WHERE players.teamname = right_teamname AND players.unum = 2 ) as r2_inertia_moment,
 	min(kick_rand) FILTER (WHERE players.teamname = right_teamname AND players.unum = 2 ) as r2_kick_rand,
 	min(kickable_margin) FILTER (WHERE players.teamname = right_teamname AND players.unum = 2 ) as r2_kickable_margin,
 	min(player_decay) FILTER (WHERE players.teamname = right_teamname AND players.unum = 2 ) as r2_player_decay,
	min(x) FILTER (WHERE players.teamname = right_teamname AND players.unum = 3 ) as r3_x,
	min(y) FILTER (WHERE players.teamname = right_teamname AND players.unum = 3 ) as r3_y,
 	min(body) FILTER (WHERE players.teamname = right_teamname AND players.unum = 3 ) as r3_body,
	min(vx) FILTER (WHERE players.teamname = right_teamname AND players.unum = 3 ) as r3_vx,
 	min(vy) FILTER (WHERE players.teamname = right_teamname AND players.unum = 3 ) as r3_vy,
	min(dash_power_rate) FILTER (WHERE players.teamname = right_teamname AND players.unum = 3 ) as r3_dash_power_rate,
	min(effort_min) FILTER (WHERE players.teamname = right_teamname AND players.unum = 3 ) as r3_effort_min,
 	min(effort_max) FILTER (WHERE players.teamname = right_teamname AND players.unum = 3 ) as r3_effort_max,
 	min(extra_stamina) FILTER (WHERE players.teamname = right_teamname AND players.unum = 3 ) as r3_extra_stamina,
 	min(inertia_moment) FILTER (WHERE players.teamname = right_teamname AND players.unum = 3 ) as r3_inertia_moment,
 	min(kick_rand) FILTER (WHERE players.teamname = right_teamname AND players.unum = 3 ) as r3_kick_rand,
 	min(kickable_margin) FILTER (WHERE players.teamname = right_teamname AND players.unum = 3 ) as r3_kickable_margin,
 	min(player_decay) FILTER (WHERE players.teamname = right_teamname AND players.unum = 3 ) as r3_player_decay,
	min(x) FILTER (WHERE players.teamname = right_teamname AND players.unum = 4 ) as r4_x,
	min(y) FILTER (WHERE players.teamname = right_teamname AND players.unum = 4 ) as r4_y,
 	min(body) FILTER (WHERE players.teamname = right_teamname AND players.unum = 4 ) as r4_body,
	min(vx) FILTER (WHERE players.teamname = right_teamname AND players.unum = 4 ) as r4_vx,
 	min(vy) FILTER (WHERE players.teamname = right_teamname AND players.unum = 4 ) as r4_vy,
	min(dash_power_rate) FILTER (WHERE players.teamname = right_teamname AND players.unum = 4 ) as r4_dash_power_rate,
	min(effort_min) FILTER (WHERE players.teamname = right_teamname AND players.unum = 4 ) as r4_effort_min,
 	min(effort_max) FILTER (WHERE players.teamname = right_teamname AND players.unum = 4 ) as r4_effort_max,
 	min(extra_stamina) FILTER (WHERE players.teamname = right_teamname AND players.unum = 4 ) as r4_extra_stamina,
 	min(inertia_moment) FILTER (WHERE players.teamname = right_teamname AND players.unum = 4 ) as r4_inertia_moment,
 	min(kick_rand) FILTER (WHERE players.teamname = right_teamname AND players.unum = 4 ) as r4_kick_rand,
 	min(kickable_margin) FILTER (WHERE players.teamname = right_teamname AND players.unum = 4 ) as r4_kickable_margin,
 	min(player_decay) FILTER (WHERE players.teamname = right_teamname AND players.unum = 4 ) as r4_player_decay,
	min(x) FILTER (WHERE players.teamname = right_teamname AND players.unum = 5 ) as r5_x,
	min(y) FILTER (WHERE players.teamname = right_teamname AND players.unum = 5 ) as r5_y,
 	min(body) FILTER (WHERE players.teamname = right_teamname AND players.unum = 5 ) as r5_body,
	min(vx) FILTER (WHERE players.teamname = right_teamname AND players.unum = 5 ) as r5_vx,
 	min(vy) FILTER (WHERE players.teamname = right_teamname AND players.unum = 5 ) as r5_vy,
	min(dash_power_rate) FILTER (WHERE players.teamname = right_teamname AND players.unum = 5 ) as r5_dash_power_rate,
	min(effort_min) FILTER (WHERE players.teamname = right_teamname AND players.unum = 5 ) as r5_effort_min,
 	min(effort_max) FILTER (WHERE players.teamname = right_teamname AND players.unum = 5 ) as r5_effort_max,
 	min(extra_stamina) FILTER (WHERE players.teamname = right_teamname AND players.unum = 5 ) as r5_extra_stamina,
 	min(inertia_moment) FILTER (WHERE players.teamname = right_teamname AND players.unum = 5 ) as r5_inertia_moment,
 	min(kick_rand) FILTER (WHERE players.teamname = right_teamname AND players.unum = 5 ) as r5_kick_rand,
 	min(kickable_margin) FILTER (WHERE players.teamname = right_teamname AND players.unum = 5 ) as r5_kickable_margin,
 	min(player_decay) FILTER (WHERE players.teamname = right_teamname AND players.unum = 5 ) as r5_player_decay,
	min(x) FILTER (WHERE players.teamname = right_teamname AND players.unum = 6 ) as r6_x,
	min(y) FILTER (WHERE players.teamname = right_teamname AND players.unum = 6 ) as r6_y,
 	min(body) FILTER (WHERE players.teamname = right_teamname AND players.unum = 6 ) as r6_body,
	min(vx) FILTER (WHERE players.teamname = right_teamname AND players.unum = 6 ) as r6_vx,
 	min(vy) FILTER (WHERE players.teamname = right_teamname AND players.unum = 6 ) as r6_vy,
	min(dash_power_rate) FILTER (WHERE players.teamname = right_teamname AND players.unum = 6 ) as r6_dash_power_rate,
	min(effort_min) FILTER (WHERE players.teamname = right_teamname AND players.unum = 6 ) as r6_effort_min,
 	min(effort_max) FILTER (WHERE players.teamname = right_teamname AND players.unum = 6 ) as r6_effort_max,
 	min(extra_stamina) FILTER (WHERE players.teamname = right_teamname AND players.unum = 6 ) as r6_extra_stamina,
 	min(inertia_moment) FILTER (WHERE players.teamname = right_teamname AND players.unum = 6 ) as r6_inertia_moment,
 	min(kick_rand) FILTER (WHERE players.teamname = right_teamname AND players.unum = 6 ) as r6_kick_rand,
 	min(kickable_margin) FILTER (WHERE players.teamname = right_teamname AND players.unum = 6 ) as r6_kickable_margin,
 	min(player_decay) FILTER (WHERE players.teamname = right_teamname AND players.unum = 6 ) as r6_player_decay,
	min(x) FILTER (WHERE players.teamname = right_teamname AND players.unum = 7 ) as r7_x,
	min(y) FILTER (WHERE players.teamname = right_teamname AND players.unum = 7 ) as r7_y,
 	min(body) FILTER (WHERE players.teamname = right_teamname AND players.unum = 7 ) as r7_body,
	min(vx) FILTER (WHERE players.teamname = right_teamname AND players.unum = 7 ) as r7_vx,
 	min(vy) FILTER (WHERE players.teamname = right_teamname AND players.unum = 7 ) as r7_vy,
	min(dash_power_rate) FILTER (WHERE players.teamname = right_teamname AND players.unum = 7 ) as r7_dash_power_rate,
	min(effort_min) FILTER (WHERE players.teamname = right_teamname AND players.unum = 7 ) as r7_effort_min,
 	min(effort_max) FILTER (WHERE players.teamname = right_teamname AND players.unum = 7 ) as r7_effort_max,
 	min(extra_stamina) FILTER (WHERE players.teamname = right_teamname AND players.unum = 7 ) as r7_extra_stamina,
 	min(inertia_moment) FILTER (WHERE players.teamname = right_teamname AND players.unum = 7 ) as r7_inertia_moment,
 	min(kick_rand) FILTER (WHERE players.teamname = right_teamname AND players.unum = 7 ) as r7_kick_rand,
 	min(kickable_margin) FILTER (WHERE players.teamname = right_teamname AND players.unum = 7 ) as r7_kickable_margin,
 	min(player_decay) FILTER (WHERE players.teamname = right_teamname AND players.unum = 7 ) as r7_player_decay,
	min(x) FILTER (WHERE players.teamname = right_teamname AND players.unum = 8 ) as r8_x,
	min(y) FILTER (WHERE players.teamname = right_teamname AND players.unum = 8 ) as r8_y,
 	min(body) FILTER (WHERE players.teamname = right_teamname AND players.unum = 8 ) as r8_body,
	min(vx) FILTER (WHERE players.teamname = right_teamname AND players.unum = 8 ) as r8_vx,
 	min(vy) FILTER (WHERE players.teamname = right_teamname AND players.unum = 8 ) as r8_vy,
	min(dash_power_rate) FILTER (WHERE players.teamname = right_teamname AND players.unum = 8 ) as r8_dash_power_rate,
	min(effort_min) FILTER (WHERE players.teamname = right_teamname AND players.unum = 8 ) as r8_effort_min,
 	min(effort_max) FILTER (WHERE players.teamname = right_teamname AND players.unum = 8 ) as r8_effort_max,
 	min(extra_stamina) FILTER (WHERE players.teamname = right_teamname AND players.unum = 8 ) as r8_extra_stamina,
 	min(inertia_moment) FILTER (WHERE players.teamname = right_teamname AND players.unum = 8 ) as r8_inertia_moment,
 	min(kick_rand) FILTER (WHERE players.teamname = right_teamname AND players.unum = 8 ) as r8_kick_rand,
 	min(kickable_margin) FILTER (WHERE players.teamname = right_teamname AND players.unum = 8 ) as r8_kickable_margin,
 	min(player_decay) FILTER (WHERE players.teamname = right_teamname AND players.unum = 8 ) as r8_player_decay,
	min(x) FILTER (WHERE players.teamname = right_teamname AND players.unum = 9 ) as r9_x,
	min(y) FILTER (WHERE players.teamname = right_teamname AND players.unum = 9 ) as r9_y,
 	min(body) FILTER (WHERE players.teamname = right_teamname AND players.unum = 9 ) as r9_body,
	min(vx) FILTER (WHERE players.teamname = right_teamname AND players.unum = 9 ) as r9_vx,
 	min(vy) FILTER (WHERE players.teamname = right_teamname AND players.unum = 9 ) as r9_vy,
	min(dash_power_rate) FILTER (WHERE players.teamname = right_teamname AND players.unum = 9 ) as r9_dash_power_rate,
	min(effort_min) FILTER (WHERE players.teamname = right_teamname AND players.unum = 9 ) as r9_effort_min,
 	min(effort_max) FILTER (WHERE players.teamname = right_teamname AND players.unum = 9 ) as r9_effort_max,
 	min(extra_stamina) FILTER (WHERE players.teamname = right_teamname AND players.unum = 9 ) as r9_extra_stamina,
 	min(inertia_moment) FILTER (WHERE players.teamname = right_teamname AND players.unum = 9 ) as r9_inertia_moment,
 	min(kick_rand) FILTER (WHERE players.teamname = right_teamname AND players.unum = 9 ) as r9_kick_rand,
 	min(kickable_margin) FILTER (WHERE players.teamname = right_teamname AND players.unum = 9 ) as r9_kickable_margin,
 	min(player_decay) FILTER (WHERE players.teamname = right_teamname AND players.unum = 9 ) as r9_player_decay,
	min(x) FILTER (WHERE players.teamname = right_teamname AND players.unum = 10 ) as r10_x,
	min(y) FILTER (WHERE players.teamname = right_teamname AND players.unum = 10 ) as r10_y,
 	min(body) FILTER (WHERE players.teamname = right_teamname AND players.unum = 10 ) as r10_body,
	min(vx) FILTER (WHERE players.teamname = right_teamname AND players.unum = 10 ) as r10_vx,
 	min(vy) FILTER (WHERE players.teamname = right_teamname AND players.unum = 10 ) as r10_vy,
	min(dash_power_rate) FILTER (WHERE players.teamname = right_teamname AND players.unum = 10 ) as r10_dash_power_rate,
	min(effort_min) FILTER (WHERE players.teamname = right_teamname AND players.unum = 10 ) as r10_effort_min,
 	min(effort_max) FILTER (WHERE players.teamname = right_teamname AND players.unum = 10 ) as r10_effort_max,
 	min(extra_stamina) FILTER (WHERE players.teamname = right_teamname AND players.unum = 10 ) as r10_extra_stamina,
 	min(inertia_moment) FILTER (WHERE players.teamname = right_teamname AND players.unum = 10 ) as r10_inertia_moment,
 	min(kick_rand) FILTER (WHERE players.teamname = right_teamname AND players.unum = 10 ) as r10_kick_rand,
 	min(kickable_margin) FILTER (WHERE players.teamname = right_teamname AND players.unum = 10 ) as r10_kickable_margin,
 	min(player_decay) FILTER (WHERE players.teamname = right_teamname AND players.unum = 10 ) as r10_player_decay,
	min(x) FILTER (WHERE players.teamname = right_teamname AND players.unum = 11 ) as r11_x,
	min(y) FILTER (WHERE players.teamname = right_teamname AND players.unum = 11 ) as r11_y,
 	min(body) FILTER (WHERE players.teamname = right_teamname AND players.unum = 11 ) as r11_body,
	min(vx) FILTER (WHERE players.teamname = right_teamname AND players.unum = 11 ) as r11_vx,
 	min(vy) FILTER (WHERE players.teamname = right_teamname AND players.unum = 11 ) as r11_vy,
	min(dash_power_rate) FILTER (WHERE players.teamname = right_teamname AND players.unum = 11 ) as r11_dash_power_rate,
	min(effort_min) FILTER (WHERE players.teamname = right_teamname AND players.unum = 11 ) as r11_effort_min,
 	min(effort_max) FILTER (WHERE players.teamname = right_teamname AND players.unum = 11 ) as r11_effort_max,
 	min(extra_stamina) FILTER (WHERE players.teamname = right_teamname AND players.unum = 11 ) as r11_extra_stamina,
 	min(inertia_moment) FILTER (WHERE players.teamname = right_teamname AND players.unum = 11 ) as r11_inertia_moment,
 	min(kick_rand) FILTER (WHERE players.teamname = right_teamname AND players.unum = 11 ) as r11_kick_rand,
 	min(kickable_margin) FILTER (WHERE players.teamname = right_teamname AND players.unum = 11 ) as r11_kickable_margin,
 	min(player_decay) FILTER (WHERE players.teamname = right_teamname AND players.unum = 11 ) as r11_player_decay
FROM matchstates as ms
	JOIN get_matchstate_playerstates(matchstate_id) as players 
		ON ms.matchstate_id = players.matchstate_id_fk
GROUP BY (
	ms.matchstate_id,
	ms.left_teamname,
	ms.right_teamname,
	ms.cycle,
 	ms.ball_x,
 	ms.ball_y,
 	ms.ball_vx,
 	ms.ball_vy
)
LIMIT batch_size;
END;

COMMIT;
